import Vue from "vue";
import App from "./App.vue";
import router from "./router";

// Apollo - GraphQL client
import VueApollo from "vue-apollo";
import { ApolloClient, InMemoryCache } from "@apollo/client/core";
import { HttpLink } from "apollo-link-http";

Vue.config.productionTip = false;
Vue.use(VueApollo);

const linkAdmin = new HttpLink({
  uri: "/graph",
  fetch,
  headers: {
    "Content-Type": "application/json",
    "X-Shopify-Access-Token": process.env.VUE_APP_SHOPIFY_TOKEN
  }
});

const linkStorefront = new HttpLink({
  uri: "https://haelpl.myshopify.com/api/graphql",
  fetch,
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
    "X-Shopify-Storefront-Access-Token":
      process.env.VUE_APP_SHOPIFY_STOREFRONT_TOKEN
  }
});

// shopify admin api
const apolloAdminClient = new ApolloClient({
  link: linkAdmin,
  cache: new InMemoryCache({
    addTypename: true
  }),
  connectToDevTools: true
});

// shopify storefront api
const apolloStorefrontClient = new ApolloClient({
  link: linkStorefront,
  cache: new InMemoryCache({
    addTypename: true
  }),
  connectToDevTools: true
});

const apolloProvider = new VueApollo({
  clients: {
    apolloAdminClient,
    apolloStorefrontClient
  },
  defaultClient: apolloAdminClient,
  errorHandler(error) {
    console.log(
      "%cError",
      "background: red; color: white; padding: 2px 4px; border-radius: 3px; font-weight: bold;",
      error.message
    );
  }
});

new Vue({
  router,
  apolloProvider: apolloProvider,
  render: h => h(App)
}).$mount("#app");
