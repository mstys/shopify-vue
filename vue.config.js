module.exports = {
  devServer: {
    port: "8081",
    proxy: {
      "/graph": {
        target: process.env.VUE_APP_GRAPHQL_HTTP,
        pathRewrite: { "^/graph": "" }
      },
      "/graph-storefront": {
        target: process.env.VUE_APP_GRAPHQL_STOREFRONT_HTTP,
        pathRewrite: { "^/graph-storefront": "" }
      }
    }
  }
};
